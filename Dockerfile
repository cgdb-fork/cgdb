FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > cgdb.log'

COPY cgdb .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' cgdb
RUN bash ./docker.sh

RUN rm --force --recursive cgdb
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD cgdb
